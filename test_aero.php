<?php

class Word
{
    function parseWords($text)
    {
        preg_match_all("#([^\s]+)#", $text, $matches);
        return $matches[1];
    }

    function getTopWords($text, $limit = 5)
    {
        $arWords = $this->parseWords($text);
        $arCounter = [];
        foreach ($arWords as $word){

            if ($arCounter[$word]) {
                $arCounter[$word]++;
            } else {
                $arCounter[$word] = 1;
            }
        }
        arsort($arCounter);
        $arCounter = array_slice($arCounter, 0, $limit, true);
        return $arCounter;
    }
}





//use
$word = new Word();
$result = $word->getTopWords('Lorem ipsum Lorem ipsum Lorem ipsum cillum dolore eu fugiat nulla pariatur.',5);

echo '<pre>';
print_r($result);
echo '</pre>';
